<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
    String path = request.getContextPath();
			String basePath = request.getScheme() + "://"
					+ request.getServerName() + ":" + request.getServerPort()
					+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>My JSP 'index.jsp' starting page</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
</head>

<body>
	<textarea rows="10" cols="80" name="xml" id="xml">

<xml>
<ToUserName><![CDATA[ssss]]></ToUserName>
<FromUserName><![CDATA[sss]]></FromUserName>
<CreateTime>1406603349</CreateTime>
<MsgType><![CDATA[text]]></MsgType>
<Content><![CDATA[deffwefwef]]></Content>
<MsgId>1234567890123456</MsgId>
</xml>
</textarea>
<br />
<div id="butts">
<button>文本</button>
<button>语音</button>
<button>图片</button>
<button>位置</button>
<button>关注</button>
<button>取消关注</button>
<button>菜单</button>
</div>
<br />
	<button id="btn">JSON测试</button>
	<button id="btn1">XML测试</button>
<br />
	<div>
		<h1>结果</h1>
		<xmp id="rs"></xmp>
	</div>
</body>
<div style="display: none;" id="xmls">
	<xmp>
<xml>
<ToUserName><![CDATA[you]]></ToUserName>
<FromUserName><![CDATA[me]]></FromUserName>
<CreateTime>1407681662</CreateTime>
<MsgType><![CDATA[text]]></MsgType>
<Content><![CDATA[dewfewfewfew]]></Content>
<MsgId>1234567890123456</MsgId>
</xml>
	</xmp>
	<xmp>
<xml>
<ToUserName><![CDATA[you]]></ToUserName>
<FromUserName><![CDATA[me]]></FromUserName>
<CreateTime>1407681808</CreateTime>
<MsgType><![CDATA[voice]]></MsgType>
<Content><![CDATA[]]></Content>
<MsgId>1234567890123456</MsgId>
</xml></xmp>
	<xmp>
<xml>
<ToUserName><![CDATA[you]]></ToUserName>
<FromUserName><![CDATA[me]]></FromUserName>
<CreateTime>1407681826</CreateTime>
<MsgType><![CDATA[image]]></MsgType>
<PicUrl><![CDATA[http://www.baidu.com]]></PicUrl>
<MsgId>1234567890123456</MsgId>
</xml>
	</xmp>
	<xmp>
<xml>
<ToUserName><![CDATA[you]]></ToUserName>
<FromUserName><![CDATA[me]]></FromUserName>
<CreateTime>1407681843</CreateTime>
<MsgType><![CDATA[location]]></MsgType>
<Location_X>111</Location_X>
<Location_Y>222</Location_Y>
<Scale>20</Scale>
</xml>
	</xmp>
	<xmp>
<xml>
<ToUserName><![CDATA[you]]></ToUserName>
<FromUserName><![CDATA[me]]></FromUserName>
<CreateTime>1407681857</CreateTime>
<MsgType><![CDATA[event]]></MsgType>
<Event><![CDATA[subscribe]]></Event>
</xml>
	</xmp>
	<xmp>
<xml>
<ToUserName><![CDATA[you]]></ToUserName>
<FromUserName><![CDATA[me]]></FromUserName>
<CreateTime>1407681864</CreateTime>
<MsgType><![CDATA[event]]></MsgType>
<Event><![CDATA[unsubscribe]]></Event>
</xml>
	</xmp>
	<xmp>
<xml>
<ToUserName><![CDATA[you]]></ToUserName>
<FromUserName><![CDATA[me]]></FromUserName>
<CreateTime>1407681880</CreateTime>
<MsgType><![CDATA[event]]></MsgType>
<Event><![CDATA[CLICK]]></Event>
<EventKey><![CDATA[thiskey]]></EventKey>
</xml>
	</xmp>
</div>
<script type="text/javascript">
var xml = $('#xml');
var xmls=$('#xmls >xmp');
var a;
var rs=$("#rs");
	var btn = $('#btn');
	var btn1 = $('#btn1');
	btn1.on('click', function() {
		var value = xml.val();
		if (value.length > 0) {
			$.ajax({
				type : 'POST',
				url : 'weixin/api.do',
				contentType : "application/xml",//application/xml  
				processData : false,//contentType为xml时，些值为false  
				dataType : "text",//json--返回json数据类型；xml--返回xml  
				data : value,
				success : function(data) {
					rs.prepend(data.replace("<", "&lt;").replace(">", "&gt;")+'\n');
				},
				error : function(e) {
				}
			});
		}
	});
	btn.on('click', function() {
		var value = xml.val();
		if (value.length > 0) {
			$.ajax({
				type : 'POST',
				/* beforeSend: function(request) {
				  request.setRequestHeader("Accept", "application/json");
				}, */
				url : 'weixin/api.do?format=json',
				contentType : "application/xml",//application/xml  
				processData : false,//contentType为xml时，些值为false  
				dataType : "html",//json--返回json数据类型；xml--返回xml  
				data : value,
				success : function(data) {
					rs.prepend(html2Escape(data)+'\n');
				},
				error : function(e) {
				}
			});
		}
	});
	$('#butts > button').each(function(i,n){
		var btn=$(n);
		btn.attr('index',i);
		btn.on('click',function(){
			xml.val(xmls[btn.attr('index')].innerHTML);
		});
	});
	function html2Escape(sHtml) {
		 return sHtml.replace(/[<>&"]/,function(c){return {'<':'&lt;','>':'&gt;','&':'&amp;','"':'&quot;'}[c];});
	}
</script>
</html>
