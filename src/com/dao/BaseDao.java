package com.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.omg.PortableInterceptor.INACTIVE;

import com.dto.Pageinfo;
import com.dto.Pagers;

public interface BaseDao<T>{
    /**
     * 保存对象
     * @param entity
     */
    public void save(T entity);
    /**
     * 删除一个对象
     * @param entity
     */
    public void del(T entity);
    /**
     * 更新一个对象
     * @param entity
     */
    public void update(T entity);
    /**
     * 通过id查找一个对象
     * @param id
     * @return
     */
    public T findById(int id);
    /**
     * 查找所有对象
     * @return
     */
    public List<T> findAll(Criterion... pamas);
    /**
     * 返回该类的总个数
     * @return
     */
    public int getCount();
    public int getCount(Criterion... pamas);
    /**
     * 分页查找
     * @param start 开始记录
     * @param limit 记录个数
     * @param order 排序
     * @param pamas 查询条件
     * @return 
     */
    public Pagers getForPage(Pageinfo pageinfo,Criterion... pamas);
    public List<T> getForPageList(Pageinfo pageinfo,Criterion... pamas);
    /**
     * hql查询
     * @param hql
     * @param pamas
     * @return
     */
    public T Quary(String hql,Object... pamas);
    public List<T> QuaryAll(String hql,Object... pamas);
    /**
     * 根据id删除一个对象
     * @param id
     */
    public void DelById(Integer id);
    /**
     * 随机获取n个对象
     * @param limit
     * @return
     */
    public List<T> getByRond(int limit);
    /**
     * 获取最后一条记录
     * @return
     */
    public T getLast();
    /**
     * 根据排序，和start，limit获取集合列表
     * @param order
     * @param start
     * @param limit
     * @return
     */
    public List<T> getList(Order order, int start,int limit);
    /**
     * 通过一个String删除实体
     * @param ids
     */
    public void DelByStringids(String ids);
    /**
     * 通过数组进行删除
     * @param ids
     */
    public void DelByIntids(Integer[] ids);
    /**
     * 返回一个hibernate查询对象
     * @param hql
     * @param pamas
     * @return
     */
    public Query getQuery(String hql,Object... pamas);
    
}
