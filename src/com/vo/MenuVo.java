package com.vo;

import java.util.List;
/**
 * menuVo 数据对象
 * MenuVo.java
 * @author  microxdd
 * @time 创建时间：2014 2014年9月14日 下午2:49:38 
 * @version
 * @user micrxdd
 */
public class MenuVo {
    private Integer id;
    private String text;
    private Boolean action;
    private String iconCls;
    private Boolean checked;
    private String url;
    private String description;
    private String role;
    private Boolean st;
    
    public Boolean getAction() {
        return action;
    }
    public void setAction(Boolean action) {
        this.action = action;
    }
    public Boolean getSt() {
        return st;
    }
    public void setSt(Boolean st) {
        this.st = st;
    }
    private List<MenuVo> children;
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getText() {
        return text;
    }
    public void setText(String text) {
        this.text = text;
    }
    public String getIconCls() {
        return iconCls;
    }
    public void setIconCls(String iconCls) {
        this.iconCls = iconCls;
    }
    public Boolean getChecked() {
        return checked;
    }
    public void setChecked(Boolean checked) {
        this.checked = checked;
    }
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getRole() {
        return role;
    }
    public void setRole(String role) {
        this.role = role;
    }
    public List<MenuVo> getChildren() {
        return children;
    }
    public void setChildren(List<MenuVo> children) {
        this.children = children;
    }
    
}
