package com.vo;

import java.util.List;

public class BaseMenuVo {
    private Integer id;
    private String text;
    private String iconCls;
    private boolean checked;
    private String state="open";
    private List<BaseMenuVo> children;
    
    public Boolean getChecked() {
        return checked;
    }
    public void setChecked(boolean checked) {
        this.checked = checked;
    }
    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getText() {
        return text;
    }
    public void setText(String text) {
        this.text = text;
    }
    public String getIconCls() {
        return iconCls;
    }
    public void setIconCls(String iconCls) {
        this.iconCls = iconCls;
    }
    public List<BaseMenuVo> getChildren() {
        return children;
    }
    public void setChildren(List<BaseMenuVo> children) {
        this.children = children;
    }
    
}
