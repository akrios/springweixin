package com.weixin.msg;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "xml")
public class TextMsg extends Msg {
	
	public TextMsg() {
		// TODO Auto-generated constructor stub
		this.msgType=MsgEmum.text.toString();
	}
	private String content;
	
	@XmlElement(name="Content")
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	
}
