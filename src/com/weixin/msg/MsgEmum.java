package com.weixin.msg;

public enum MsgEmum {
	df("df"),
	text("text"),
	image("image"),
    	video("video"),
    	voice("voice"),
    	location("location"),
	event("event"),
	music("music"),
	news("news");
	private String msgType;
	MsgEmum(String string){
		this.msgType=string;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.msgType;
	}
	
}
