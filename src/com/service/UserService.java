package com.service;

import java.util.List;

import com.dto.Pageinfo;
import com.dto.Pagers;
import com.dto.UserDto;
import com.entity.User;
/**
 * 用户服务接口
 * UserService.java
 * @author  microxdd
 * @time 创建时间：2014 2014年9月14日 下午3:01:22 
 * @version
 * @user micrxdd
 */
public interface UserService {
    /**
     * 根据用户名查找用户
     * @param username
     * @return
     */
    public User findUser(String username);
    /**
     * 返回用户列表
     * @param pageinfo
     * @return
     */
    public Pagers UserList(Pageinfo pageinfo);
    /**
     * 保存用户 默认密码11111111
     * @param dtos
     */
    public void SaveUsers(List<UserDto> dtos);
    /**
     * 更新用户
     * @param dtos
     */
    public void UpdateUsers(List<UserDto> dtos);
    /**
     * 删除用户
     * @param ids
     */
    public void DelUserIds(Integer[] ids);
}
