package com.service;

import java.util.List;

import com.dto.MsgprsDto;
import com.dto.Pageinfo;
import com.dto.Pagers;
/**
 * 消息分发类接口
 * MsgprsService.java
 * @author  microxdd
 * @time 创建时间：2014 2014年9月14日 下午2:28:16 
 * @version
 * @user micrxdd
 */
public interface MsgprsService {
    /**
     * 返回消息分发接口
     * @param pageinfo
     * @return
     */
    public Pagers MsgprsList(Pageinfo pageinfo);
    /**
     * 保存消息分发
     * @param dtos
     */
    public void SaveMsgprs(List<MsgprsDto> dtos);
    /**
     * 更新消息分发
     * @param dtos
     */
    public void UpdateMsgprs(List<MsgprsDto> dtos);
    /**
     * 删除消息分发
     * @param ids
     */
    public void DelMsgprs(String ids);
    /**
     * 删除消息分发
     * @param ids
     */
    public void DelMsgIntprs(Integer[] ids);
}
