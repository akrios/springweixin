package com.entity;

import java.util.HashSet;
import java.util.Set;

/**
 * Weimenu entity. @author MyEclipse Persistence Tools
 */

public class Weimenu implements java.io.Serializable {

    // Fields

    private Integer id;
    private Weimenu weimenu;
    private String name;
    private String type;
    private String key;
    private String url;
    private Integer msgprsid;
    private Boolean enable;
    private Set weimenus = new HashSet(0);

    // Constructors

    /** default constructor */
    public Weimenu() {
    }

    /** full constructor */
    public Weimenu(Weimenu weimenu, String name, String type, String key,
	    String url, Integer msgprsid, Boolean enable, Set weimenus) {
	this.weimenu = weimenu;
	this.name = name;
	this.type = type;
	this.key = key;
	this.url = url;
	this.msgprsid = msgprsid;
	this.enable = enable;
	this.weimenus = weimenus;
    }

    // Property accessors

    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public Weimenu getWeimenu() {
	return this.weimenu;
    }

    public void setWeimenu(Weimenu weimenu) {
	this.weimenu = weimenu;
    }

    public String getName() {
	return this.name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String getType() {
	return this.type;
    }

    public void setType(String type) {
	this.type = type;
    }

    public String getKey() {
	return this.key;
    }

    public void setKey(String key) {
	this.key = key;
    }

    public String getUrl() {
	return this.url;
    }

    public void setUrl(String url) {
	this.url = url;
    }

    public Integer getMsgprsid() {
	return this.msgprsid;
    }

    public void setMsgprsid(Integer msgprsid) {
	this.msgprsid = msgprsid;
    }

    public Boolean getEnable() {
	return this.enable;
    }

    public void setEnable(Boolean enable) {
	this.enable = enable;
    }

    public Set getWeimenus() {
	return this.weimenus;
    }

    public void setWeimenus(Set weimenus) {
	this.weimenus = weimenus;
    }

}