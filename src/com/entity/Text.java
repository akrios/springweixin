package com.entity;

import java.sql.Timestamp;

/**
 * Text entity. @author MyEclipse Persistence Tools
 */

public class Text implements java.io.Serializable {

    // Fields

    private Integer id;
    private User user;
    private String text;
    private Timestamp time;
    private Timestamp edittime;

    // Constructors

    /** default constructor */
    public Text() {
    }

    /** full constructor */
    public Text(User user, String text, Timestamp time, Timestamp edittime) {
	this.user = user;
	this.text = text;
	this.time = time;
	this.edittime = edittime;
    }

    // Property accessors

    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public User getUser() {
	return this.user;
    }

    public void setUser(User user) {
	this.user = user;
    }

    public String getText() {
	return this.text;
    }

    public void setText(String text) {
	this.text = text;
    }

    public Timestamp getTime() {
	return this.time;
    }

    public void setTime(Timestamp time) {
	this.time = time;
    }

    public Timestamp getEdittime() {
	return this.edittime;
    }

    public void setEdittime(Timestamp edittime) {
	this.edittime = edittime;
    }

}